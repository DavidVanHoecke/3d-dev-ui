using UnityEngine;
using UnityEngine.Events;

#if ENABLE_INPUT_SYSTEM
using UnityEngine.InputSystem;
#endif

namespace StarterAssets
{
	public class StarterAssetsInputs : MonoBehaviour
	{
		public UnityEvent OnJumpEvent = new UnityEvent();
        public UnityEvent OnDodgeEvent = new UnityEvent();

        [Header("Character Input Values")]
		public Vector2 move;
		public Vector2 look;
		public bool jump;
		public bool sprint;
		public bool staminaDrained = false;
		public bool dodge;

        [Header("Movement Settings")]
		public bool analogMovement;

		[Header("Mouse Cursor Settings")]
		public bool cursorLocked = true;
		public bool cursorInputForLook = true;


        public void Start()
        {
        }


#if ENABLE_INPUT_SYSTEM
        public void OnMove(InputValue value)
		{
			MoveInput(value.Get<Vector2>());
		}

		public void OnLook(InputValue value)
		{
			if(cursorInputForLook)
			{
				LookInput(value.Get<Vector2>());
			}
		}

		public void OnJump(InputValue value)
		{
			JumpInput(value.isPressed);
			OnJumpEvent.Invoke();

        }

		public void OnSprint(InputValue value)
		{
			SprintInput(value.isPressed);
		}

		public void OnDodge(InputValue value)
        {
			Debug.Log("Dodge");

			if(staminaDrained)
			{ 
				return;
			}

            dodge = value.isPressed;
			if (dodge)
			{
				OnDodgeEvent.Invoke();
            }

			Debug.Log(value);
        }
#endif


        public void MoveInput(Vector2 newMoveDirection)
		{
			move = newMoveDirection;
		} 

		public void LookInput(Vector2 newLookDirection)
		{
			look = newLookDirection;
		}

		public void JumpInput(bool newJumpState)
		{
			if (staminaDrained)
				return;

			jump = newJumpState;
		}

		public void SprintInput(bool newSprintState)
		{
			if(staminaDrained)
                newSprintState = false;

			if (sprint != newSprintState)
				Debug.Log(newSprintState);

            sprint = newSprintState;
		}

		private void OnApplicationFocus(bool hasFocus)
		{
			SetCursorState(cursorLocked);
		}

		private void SetCursorState(bool newState)
		{
			Cursor.lockState = newState ? CursorLockMode.Locked : CursorLockMode.None;
		}
	}
	
}