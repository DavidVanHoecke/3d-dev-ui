using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManger : MonoBehaviour
{
    public GameObject player;

    public List<EnemyNav> enemies = new List<EnemyNav>();
    
    public List<GameObject> spawnPoints= new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnEnemies());
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    private IEnumerator SpawnEnemies()
    {
        while (true)
        {
            yield return new WaitForSeconds(5);
            SpawnEnemy();
        }
    }

    private void SpawnEnemy()
    {
        if(enemies.Count > 0)
        {
            int rndNbr = UnityEngine.Random.Range(0, enemies.Count);

            GameObject enemyToSpawn = enemies[rndNbr].gameObject;

            if(spawnPoints.Count > 0)
            {
                rndNbr = UnityEngine.Random.Range(0, spawnPoints.Count);

                Transform pointToSpawn = spawnPoints[rndNbr].transform;

                GameObject newEnemy = GameObject.Instantiate(enemyToSpawn, pointToSpawn);
                newEnemy.transform.parent = null;
                newEnemy.GetComponent<EnemyNav>().target = player.transform;
            }
        }
    }
}
