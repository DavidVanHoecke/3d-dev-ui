using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerCompass : MonoBehaviour
{
    [SerializeField]
    RectTransform compassBar;

    [SerializeField]
    RectTransform markerNorth;

    [SerializeField]
    RectTransform markerSouth;

    [SerializeField]
    RectTransform markerEast;

    [SerializeField]
    RectTransform markerWest;

    [SerializeField]
    RectTransform markerObjective;

    [SerializeField]
    Transform transformCamera;

    [SerializeField]
    Transform transformObjective;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        SetMarkerPosition(markerObjective, transformObjective.position);
        SetMarkerPosition(markerNorth, Vector3.forward * 1000);
        SetMarkerPosition(markerSouth, Vector3.back * 1000);
        SetMarkerPosition(markerEast, Vector3.right* 1000);
        SetMarkerPosition(markerWest, Vector3.left * 1000);
    }

    void SetMarkerPosition(RectTransform markerTransform, Vector3 worldPosition)
    {
        Vector3 direction = worldPosition - transformCamera.position;
        float angle = Vector2.SignedAngle(new Vector2(direction.x, direction.z), new Vector2(transformCamera.transform.forward.x, transformCamera.transform.forward.z));
        float compassPositionX = Mathf.Clamp(2 * angle / Camera.main.fieldOfView, -1, 1);
        markerTransform.anchoredPosition = new Vector2(compassPositionX * compassBar.rect.width / 2, 0);
    }
}
