using StarterAssets;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ManagerUI : MonoBehaviour
{
    [SerializeField]
    private Slider sliderHP;

    [SerializeField]
    private Slider sliderMP;

    [SerializeField]
    private Slider sliderStamina;

    [SerializeField]
    private TextMeshProUGUI textHP;

    [SerializeField]
    private TextMeshProUGUI textMP;

    [SerializeField]
    private TextMeshProUGUI textStamina;

    [SerializeField]
    StarterAssetsInputs starterAssetsInputs;

    [SerializeField]
    [Range(1, 100)]
    float staminaDrain = 100.0f;
    [SerializeField]
    [Range(1, 10)]
    float staminaDrainTimeout = 2.0f;
    [SerializeField]
    [Range(1, 50)]
    private float jumpDrain = 20.0f;

    int maxHP = 0;
    int maxMP = 0;
    int maxStamina = 0;

    float lastStaminaDrain = 0.0f;

    string tmpValue = string.Empty;
    

    // Start is called before the first frame update
    void Start()
    {
        sliderHP.onValueChanged.AddListener(OnSliderChangedHP);
        sliderMP.onValueChanged.AddListener(OnSliderChangedMP);
        sliderStamina.onValueChanged.AddListener(OnSliderChangedStamina);
        starterAssetsInputs.OnJumpEvent.AddListener(DrainStaminaByJumping);
        starterAssetsInputs.OnDodgeEvent.AddListener(DrainStaminaByDodging);

        SetMaxima();
        InitTextFields();
    }

    private void DrainStaminaByDodging()
    {
        Debug.Log("Drain Stamina by dodging");
        lastStaminaDrain = Time.time;
        sliderStamina.value -= jumpDrain;
    }

    void Update()
    {
        // drain stamina when sprinting
        if (starterAssetsInputs.sprint)
        {
            sliderStamina.value -= staminaDrain * Time.deltaTime;
            lastStaminaDrain = Time.time;
        }
        else
        {
            if (Time.time - lastStaminaDrain > staminaDrainTimeout) // only regain stamina after a timeout
            {
                Debug.Log("regain stamina");
                sliderStamina.value += staminaDrain * Time.deltaTime;
            }
            
        }

        if (sliderStamina.value < 0)
        {
            sliderStamina.value = 0;
        }

        if(sliderStamina.value == 0) 
        { 
            starterAssetsInputs.staminaDrained = true;
            starterAssetsInputs.SprintInput(false);
        }
        else
        {
            starterAssetsInputs.staminaDrained = false;
        }
    }

    private void SetMaxima()
    {
        maxHP = (int)sliderHP.maxValue;
        maxMP = (int)sliderMP.maxValue;
        maxStamina = (int)sliderStamina.maxValue;
    }

    private void InitTextFields()
    {
        OnSliderChangedHP(255);
        OnSliderChangedMP(maxMP);
        OnSliderChangedStamina(maxStamina);
    }


    private void DrainStaminaByJumping()
    {
        Debug.Log("Drain Stamina by Jumping");
        lastStaminaDrain = Time.time;
        sliderStamina.value -= jumpDrain;
    }

    private void OnSliderChangedHP(float number)
    {
        tmpValue = $"{number.ToString()}/{maxHP}";
        if (textHP.text != tmpValue)
        {
            textHP.text = tmpValue;
        }
    }

    private void OnSliderChangedMP(float number)
    {
        tmpValue = $"{number.ToString()}/{maxMP}";
        if (textMP.text != tmpValue)
        {
            textMP.text = tmpValue;
        }
    }

    private void OnSliderChangedStamina(float number)
    {
        tmpValue = $"{number.ToString("0")}/{maxStamina}";
        if (textStamina.text != tmpValue)
        {
            textStamina.text = tmpValue;
        }
    }
}
